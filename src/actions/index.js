import { normalize } from "normalizr";
import * as schema from "./schema";
import * as api from "../api";
import { getIsFetching } from "../reducers/orders";

export const fetchOrders = () => (dispatch, getState) => {
  if (getIsFetching(getState())) {
    return Promise.resolve;
  }

  dispatch({
    type: "FETCH_ORDERS_REQUEST"
  });
  return (
    api
      .fetchOrders()
      .then(response => {
        console.log("fetchOrders response", response);
        dispatch({
          type: "FETCH_ORDERS_SUCCESS",
          response: normalize(response, schema.orderSchema)
        });
      })
      //TODO:
      // additional processing of response status looks redundant,
      // need to investigate more deeply about resolving promises in case of HTTP errors with enabled CORS.
      // Presently in case of some HTTP error (401, for example) fetch doesn't reject a promise as expected.
      // https://github.com/github/fetch/issues/201
      // One of the possible solutions (just assumption, not tested yet): manual promise rejection on the api.doRequest level.
      .catch(error => {
        console.log("error", error);
        if (error.status === 401) {
          dispatch({
            type: "FETCH_ORDERS_FAILURE",
            message: error.message
          });
        } else {
          dispatch({
            type: "FETCH_ORDERS_FAILURE",
            message: `Something went wrong. Error Code=${error.status}`
          });
        }
      })
  );
};
