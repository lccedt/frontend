const ids = (state = [], action) => {
  switch (action.type) {
    case "FETCH_ORDERS_SUCCESS":
      return action.response.result;
    default:
      return state;
  }
};

export default ids;
