import { combineReducers } from "redux";
import * as fromById from "./byId";
import ids from "./orderIds";

const orders = combineReducers({
  byId: fromById.byId,
  isFetching: fromById.isFetching,
  isErrorMessage: fromById.errorMessage,
  ids
});

export default orders;

export const getAllOrders = state => {
  const ids = state.orders.ids;
  // ids.map(id => console.log("id=", id, state.orders.byId));
  return ids.map(id => state.orders.byId[id]); //TODO: encapsulate to the byId
};
export const getIsFetching = state => state.orders.isFetching;
export const getErrorMessage = state => state.orders.isErrorMessage;
