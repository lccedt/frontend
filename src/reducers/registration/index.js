import { combineReducers } from "redux";
import _ from "lodash";

export const isRegistered = (state = false, action) => {
  switch (action.type) {
    case "SIGNUP_SUCCESS":
      return true;
    case "SIGNUP_FAILURE":
      return state;
    default:
      return _.isEmpty(localStorage.getItem("accessToken")) ? state : true;
  }
};

export const registrationError = (state = {}, action) => {
  switch (action.type) {
    case "SIGNUP_SUCCESS":
      return {};
    case "SIGNUP_FAILURE":
      return {
        errorCode: action.response.status,
        errorMessage: action.response.message
      };
    default:
      return state;
  }
};

const userRegistration = combineReducers({
  isRegistered,
  registrationError,
});

export default userRegistration;

export const getIsRegistered = state => state.userRegistration.isRegistered;
export const getRegistrationError = state =>
  state.userRegistration.registrationError;
