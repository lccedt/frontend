import orders from "./orders";
import actualUser from "./authentication";
import userRegistration from "./registration";
import { combineReducers } from "redux";

const mainReducer = combineReducers({
  orders,
  actualUser,
  userRegistration,
});

export default mainReducer;
