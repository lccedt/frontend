import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter, Redirect } from "react-router-dom";
import _ from "lodash";
import "./Login.css";
import { Link } from "react-router-dom";
import { Form, Input, Button, Icon, notification } from "antd";
import * as actions from "../../actions/user";
import {
  getIsAuthenticated,
  getAuthenticationError,
  getUser
} from "../../reducers/authentication";

const FormItem = Form.Item;

class Login extends Component {

  render() {
    console.log("greetings from the login render");

    const { isAuthenticated, authenticationError } = this.props;

    if (isAuthenticated) {
      this.props.onLoginSuccess();
      return <Redirect to="/" />;
    }

    if (!_.isEmpty(authenticationError)) {
      this.props.onLoginFailed();
    }

    const AntWrappedLoginForm = Form.create()(LoginForm);
    return (
      <div className="login-container">
        <h1 className="page-title">Login</h1>
        <div className="login-content">
          <AntWrappedLoginForm {...this.props} />
        </div>
      </div>
    );
  }
}

class LoginForm extends Component {
  handleSubmit = event => {
    console.log("handleSubmit");
    event.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        const loginRequest = { ...values };
        const { getAuthenticatedUser } = this.props;
        getAuthenticatedUser(loginRequest);
      }
    });
  };

  render() {
    console.log("greetings from the loginform render");
    const { getFieldDecorator } = this.props.form;

    return (
      <Form onSubmit={this.handleSubmit} className="login-form">
        <FormItem>
          {getFieldDecorator("username", {
            rules: [
              {
                required: true,
                message: "Please input your username!"
              }
            ]
          })(
            <Input
              prefix={<Icon type="user" />}
              size="large"
              name="username"
              placeholder="Username"
            />
          )}
        </FormItem>
        <FormItem>
          {getFieldDecorator("password", {
            rules: [{ required: true, message: "Please input your Password!" }]
          })(
            <Input
              prefix={<Icon type="lock" />}
              size="large"
              name="password"
              type="password"
              placeholder="Password"
            />
          )}
        </FormItem>
        <FormItem>
          <Button
            type="primary"
            htmlType="submit"
            size="large"
            className="login-form-button"
          >
            Login
          </Button>
          Or <Link to="/signup">register now!</Link>
        </FormItem>
      </Form>
    );
  }
}

const mapStateToProps = state => {
  return {
    isAuthenticated: getIsAuthenticated(state),
    authenticationError: getAuthenticationError(state),
    authenticatedUser: getUser(state)
  };
};

Login = withRouter(
  connect(
    mapStateToProps,
    actions
  )(Login)
);

export default Login;
