import React from "react";

const Order = ({ startPoint, endPoint }) => (
  <div>
    <li>Откуда: {startPoint}</li>
    <li>Куда: {endPoint}</li>
  </div>
);

export default Order;
