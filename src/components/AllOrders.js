import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import * as actions from "../actions";
import _ from "lodash";
import OrderList from "./OrderList";
import { YMaps, Map, Placemark } from "react-yandex-maps";

import {
  getAllOrders,
  getErrorMessage,
  getIsFetching
} from "../reducers/orders";
import LoadingIndicator from "./common/LoadingIndicator";

class AllOrders extends Component {
  componentDidMount() {
    console.log("greetings from allorders componentdidmount");
    this.fetchData();
  }

  fetchData() {
    const { fetchOrders } = this.props;
    fetchOrders();
  }

  render() {
    console.log("greetings from the allorders render");
    const { isFetching, errorMessage, orders } = this.props;
    if (isFetching && !orders.length) {
      return <LoadingIndicator />;
    }

    if (errorMessage) {
      return <p>{errorMessage}</p>;
    }
    if (!_.isEmpty(orders)) {
      console.log("orders: ", orders);
    }
    return (
      <div>
        <OrderList orders={orders} />
        {/* <button
          onClick={e => {
            this.fetchData();
          }}
        >
          push
        </button> */}
        <YMaps>
          <Map
            state={{ center: [51.680671, 39.118298], zoom: 14 }}
            width="100%"
            height="400px"
          >
            <Placemark
              geometry={{
                coordinates: [51.680671, 39.118298]
              }}
              properties={{
                hintContent: "9 Января 233/32",
                balloonContent: "Доставить сюда"
              }}
              // options={{
              //   iconLayout: "default#image",
              //   iconImageHref: "images/myIcon.gif",
              //   iconImageSize: [30, 42],
              //   iconImageOffset: [-3, -42]
              // }}
            />
            <Placemark geometry={{ coordinates: [51.654489, 39.192696] }} />
          </Map>
        </YMaps>
      </div>
    );
    // return <p>asd</p>;
  }
}

const mapStateToProps = state => {
  return {
    isFetching: getIsFetching(state),
    errorMessage: getErrorMessage(state),
    orders: getAllOrders(state)
  };
};

AllOrders = withRouter(
  connect(
    mapStateToProps,
    actions
  )(AllOrders)
);

export default AllOrders;
