import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter, Link, Redirect } from "react-router-dom";
import * as actions from "../actions/user";
import _ from "lodash";
import "./Signup.css";

import {
  getIsRegistered,
  getRegistrationError
} from "../reducers/registration";

import { Form, Input, Button, Radio } from "antd";
const FormItem = Form.Item;

class Signup extends Component {
  render() {
    const { isRegistered, registrationError } = this.props;

    if (isRegistered) {
      this.props.onSignupSuccess();
      return <Redirect to="/login" />;
    }

    if (!_.isEmpty(registrationError)) {
      this.props.onSignupFailed();
    }

    const WrappedSignupFrom = Form.create()(SignupForm);

    return (
      <div className="signup-container">
        <h1 className="page-title">Sign Up</h1>
        <div className="signup-content">
          <WrappedSignupFrom {...this.props} />
        </div>
      </div>
    );
  }
}

class SignupForm extends Component {
  handleSubmit = event => {
    event.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        const signUpRequest = { ...values };
        const { signUpNewUser } = this.props;
        console.log("signup request", signUpRequest);
        signUpNewUser(signUpRequest);
      }
    });
  };

  render() {
    const { getFieldDecorator } = this.props.form;

    return (
      <Form onSubmit={this.handleSubmit} className="signup-form">
        <FormItem label="Full Name" hasFeedback>
          {getFieldDecorator("name", {
            rules: [
              {
                required: true,
                message: "Please input your name!"
              }
            ]
          })(
            <Input
              size="large"
              name="name"
              autoComplete="off"
              placeholder="Your full name"
            />
          )}
        </FormItem>
        <FormItem label="Username" hasFeedback>
          {getFieldDecorator("username", {
            rules: [
              {
                required: true,
                message: "Please input your name!"
              }
            ]
          })(
            <Input
              size="large"
              name="username"
              autoComplete="off"
              placeholder="A unique username"
            />
          )}
        </FormItem>
        <FormItem label="User type">
          {getFieldDecorator("userRole", {
            rules: [
              {
                required: true,
                message: "Please input your name!"
              }
            ]
          })(
            <Radio.Group>
              <Radio.Button value="SUPPLIER">Supplier</Radio.Button>
              <Radio.Button value="COURIER">Courier</Radio.Button>
            </Radio.Group>
          )}
        </FormItem>
        <FormItem label="Email" hasFeedback>
          {getFieldDecorator("email", {
            rules: [
              {
                required: true,
                message: "Please input your name!"
              }
            ]
          })(
            <Input
              size="large"
              name="email"
              type="email"
              autoComplete="off"
              placeholder="Your email"
            />
          )}
        </FormItem>
        <FormItem label="Password">
          {getFieldDecorator("password", {
            rules: [
              {
                required: true,
                message: "Please input your name!"
              }
            ]
          })(
            <Input
              size="large"
              name="password"
              type="password"
              autoComplete="off"
              placeholder="A password between 6 to 20 characters"
            />
          )}
        </FormItem>
        <FormItem>
          <Button
            type="primary"
            htmlType="submit"
            size="large"
            className="signup-form-button"
          >
            Sign up
          </Button>
          Already registed? <Link to="/login">Login now!</Link>
        </FormItem>
      </Form>
    );
  }
}

const mapStateToProps = state => {
  return {
    isRegistered: getIsRegistered(state),
    errorMessage: getRegistrationError(state)
  };
};

Signup = withRouter(
  connect(
    mapStateToProps,
    actions
  )(Signup)
);

export default Signup;
